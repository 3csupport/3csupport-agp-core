package com.prosa.app.agp.controller;

import com.prosa.app.agp.dao.BitacoraDAO;
import com.prosa.app.agp.model.Bitacora;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;








@Component("bitacoraController")
public class BitacoraController
{
  public Bitacora getBitacora(HttpServletRequest hsr)
  {
    Bitacora bitacora = new Bitacora();
    try {
      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
      Date date = new Date();
      String usuario = System.getenv().get("USERNAME") == null ? System.getProperty("user.name") : (String)System.getenv().get("USERNAME");
      InetAddress IP = InetAddress.getLocalHost();
      String ipAdress = IP.getHostAddress();
      String hostname = IP.getHostName();
      StringBuffer url = hsr.getRequestURL();
      bitacora.setFecEvento(date);
      bitacora.setNombreAplicativo("AGP");
      bitacora.setIpAplicativo(url.toString());
      bitacora.setHostname(hostname);
      bitacora.setIpEquipoUsuario(ipAdress);
      bitacora.setIdUsuario(usuario);
    }
    catch (Exception localException) {}
    return bitacora;
  }
  
  public void addBitacora(Bitacora bitacora) throws Exception {
    try {
      this.bitacoraDAO.inserta(bitacora);
    }
    catch (Exception localException) {}
  }
  
  public Bitacora getBitacoraInserta(HttpServletRequest hsrAlta) {
    Bitacora bitacora = null;
    try {
      bitacora = getBitacora(hsrAlta);
      bitacora.setNombreCortoEvento("Inserta");
      bitacora.setReporte("Inserta agregador");
      bitacora.setDetalleEvento("Se inserta registro en la tabla TBL_AGP_AGREGADORES, se genera un response de AgregadorResponse");
    }
    catch (Exception e) {
      log.error("Error al generar bitacora");
    }
    return bitacora;
  }
  
  public Bitacora getBitacoraActualiza(HttpServletRequest hsrModifica) {
    Bitacora bitacora = null;
    try {
      bitacora = getBitacora(hsrModifica);
      bitacora.setNombreCortoEvento("Actualiza");
      bitacora.setReporte("Actualiza agregador");
      bitacora.setDetalleEvento("Se actualiza registro en la tabla TBL_AGP_AGREGADORES, se genera un response de AgregadorResponse");
    }
    catch (Exception e) {
      log.error("Error al generar bitacora");
    }
    return bitacora;
  }
  
  public Bitacora getBitacoraConsulta(HttpServletRequest hsrConsulta) {
    Bitacora bitacora = null;
    try {
      bitacora = getBitacora(hsrConsulta);
      bitacora.setNombreCortoEvento("Consulta");
      bitacora.setReporte("Consulta agregador");
      bitacora.setDetalleEvento("Se consulta registro en la tabla TBL_AGP_AGREGADORES, se genera un response de AgregadorResponse");
    }
    catch (Exception e) {
      log.error("Error al generar bitacora");
    }
    return bitacora;
  }
  
  public Bitacora getBitacoraCancela(HttpServletRequest hsrCancela) {
    Bitacora bitacora = null;
    try {
      bitacora = getBitacora(hsrCancela);
      bitacora.setNombreCortoEvento("Cancela");
      bitacora.setReporte("Cancela agregador");
      bitacora.setDetalleEvento("Se actualiza estatus a cancelado en la tabla TBL_AGP_AGREGADORES, se genera un response de cancelacion exitosa");
    }
    catch (Exception e) {
      log.error("Error al generar bitacora");
    }
    return bitacora;
  }
  
  public Bitacora getBitacoraBloqueo(HttpServletRequest hsrBloquea) {
    Bitacora bitacora = null;
    try {
      bitacora = getBitacora(hsrBloquea);
      bitacora.setNombreCortoEvento("Bloqueo");
      bitacora.setReporte("Bloqueo agregador");
      bitacora.setDetalleEvento("Se bloquea un agregador asignando el estatus bloqueado en la tabla TBL_AGP_AGREGADORES, se genera una respuesta de bloqueo exitoso");
    }
    catch (Exception e) {
      log.error("Error al generar bitacora");
    }
    return bitacora;
  }
  
  public String getCadena(Bitacora bitacora) {
    DateFormat df = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
    String fecEventoFormat = df.format(Long.valueOf(bitacora.getFecEvento().getTime()));
    return this.cadena = fecEventoFormat + "|" + bitacora.getNombreAplicativo() + "|" + bitacora.getIpAplicativo() + "|" + bitacora.getHostname() + "|" + bitacora.getIpEquipoUsuario() + "|" + bitacora.getIdUsuario() + "|" + bitacora.getNombreCortoEvento() + "|" + bitacora.getReporte() + "|" + bitacora.getDetalleEvento();
  }
  

  private static final Logger log = Logger.getLogger(BitacoraController.class);
  private static final String separator = "|";
  String cadena;
  @Autowired
  BitacoraDAO bitacoraDAO;
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\controller\BitacoraController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */