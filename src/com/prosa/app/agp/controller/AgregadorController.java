package com.prosa.app.agp.controller;

import com.prosa.app.agp.dao.AgregadorDAO;
import com.prosa.app.agp.model.Agregador;
import com.prosa.app.agp.model.AgregadorResponseList;
//import com.prosa.app.agp.utils.LogClass;
import com.prosa.app.agp.utils.LogMonitoreo;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;





@Component("agregadorController")
public class AgregadorController
{
  private static final Logger logger = Logger.getLogger(AgregadorController.class);
  
  @Autowired
  AgregadorDAO agregadorDAO;
  
  @Autowired
  //LogClass log;
  
  @Value("${ruta.bitacora.log}")
  private String ruta;
  
  @Value("${subAfiliados.log.activa.segundos}")
  private Integer tiempo;
  
  @Value("${subAfiliados.log.activa.pass}")
  private String activaPass;
  
  @PostConstruct
  public void init()
  {
    logger.info("Inicia postConstruct");
    new LogMonitoreo(this.tiempo, this.activaPass);
  }
  
  public void addAgregador(Agregador agregador) throws Exception {
    List<ConstraintViolation> violations = null;
    Validator validator = new Validator();
    String mensaje = "";
    try
    {
      ConstraintViolation constraintViolation;
      for (Iterator var5 = validator.validate(agregador).iterator(); var5.hasNext(); mensaje = mensaje + constraintViolation.getMessage()) {
        constraintViolation = (ConstraintViolation)var5.next();
      }
      
      if (mensaje.equals("")) {
        this.agregadorDAO.inserta(agregador);
      } else {
        agregador.setMensaje(mensaje);
        agregador.setStatus("ERROR");
      }
    }
    catch (Exception var7) {
      throw new Exception("Error al insertar la tabla TBL_AGP_AGREGADOR [{}]", var7);
    }
  }
  
  public void updAgregador(Agregador agregador) throws Exception {
    List<ConstraintViolation> violations = null;
    Validator validator = new Validator();
    String mensaje = "";
    try
    {
      ConstraintViolation constraintViolation;
      for (Iterator var5 = validator.validate(agregador).iterator(); var5.hasNext(); mensaje = mensaje + constraintViolation.getMessage()) {
        constraintViolation = (ConstraintViolation)var5.next();
      }
      
      if (mensaje.equals("")) {
        this.agregadorDAO.actualiza(agregador);
      } else {
        agregador.setMensaje(mensaje);
        agregador.setStatus("ERROR");
      }
    }
    catch (Exception var7) {
      throw new Exception("Error al actualizar la tabla TBL_AGP_AGREGADOR [{}]", var7);
    }
  }
  
  public AgregadorResponseList selAgregador(String nombre, Integer afiliacion, String rfc) throws Exception {
    AgregadorResponseList agregadores = null;
    try
    {
      if ((nombre != null) && (afiliacion != null) && (rfc != null)) {
        return this.agregadorDAO.listaAgregadores(nombre, afiliacion, rfc);
      }
      
      agregadores.setMensaje("La búsqueda debe realizarse por nombre, afiliación y rfc");
      agregadores.setStatus("ERROR");
      return agregadores;
    }
    catch (Exception var6) {
      throw new Exception("Error al consultar agregadores");
    }
  }
  
  public Agregador cancelaAgregador(String banco, Integer afiliacion, Integer agregador) throws Exception {
    Agregador agregadores = null;
    try
    {
      return this.agregadorDAO.cancela(banco, afiliacion, agregador);
    }
    catch (Exception var6) {
      throw new Exception("Error al cancelar agregador [{}]", var6);
    }
  }
  
  public Agregador bloqueaAgregador(String banco, Integer afiliacion, Integer agregador) throws Exception {
    Agregador agregadores = null;
    try
    {
      return this.agregadorDAO.bloquea(banco, afiliacion, agregador);
    }
    catch (Exception var6) {
      throw new Exception("Error al bloquear agregador [{}]", var6);
    }
  }
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\controller\AgregadorController.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */