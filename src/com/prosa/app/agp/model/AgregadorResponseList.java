package com.prosa.app.agp.model;

import java.util.List;
import org.springframework.stereotype.Component;







@Component("responseAgregadorList")
public class AgregadorResponseList
{
  private String Status;
  private String Mensaje;
  private List<AgregadorResponse> listAgregadores;
  
  public String getStatus()
  {
    return this.Status;
  }
  
  public void setStatus(String status) {
    this.Status = status;
  }
  
  public String getMensaje() {
    return this.Mensaje;
  }
  
  public void setMensaje(String mensaje) {
    this.Mensaje = mensaje;
  }
  
  public List<AgregadorResponse> getListAgregadores() {
    return this.listAgregadores;
  }
  
  public void setListAgregadores(List<AgregadorResponse> listAgregadores) {
    this.listAgregadores = listAgregadores;
  }
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\model\AgregadorResponseList.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */