package com.prosa.app.agp.model;

import java.util.Date;
import org.springframework.stereotype.Component;




@Component("bitacora")
public class Bitacora
{
  private Integer idBitacora;
  private Date fecEvento;
  private String nombreAplicativo;
  private String ipAplicativo;
  private String hostname;
  private String ipEquipoUsuario;
  private String nombreCortoEvento;
  private String reporte;
  private String idUsuario;
  private String detalleEvento;
  
  public Integer getIdBitacora()
  {
    return this.idBitacora;
  }
  
  public void setIdBitacora(Integer idBitacora) {
    this.idBitacora = idBitacora;
  }
  
  public Date getFecEvento() {
    return this.fecEvento;
  }
  
  public void setFecEvento(Date fecEvento) {
    this.fecEvento = fecEvento;
  }
  
  public String getNombreAplicativo() {
    return this.nombreAplicativo;
  }
  
  public void setNombreAplicativo(String nombreAplicativo) {
    this.nombreAplicativo = nombreAplicativo;
  }
  
  public String getIpAplicativo() {
    return this.ipAplicativo;
  }
  
  public void setIpAplicativo(String ipAplicativo) {
    this.ipAplicativo = ipAplicativo;
  }
  
  public String getHostname() {
    return this.hostname;
  }
  
  public void setHostname(String hostname) {
    this.hostname = hostname;
  }
  
  public String getIpEquipoUsuario() {
    return this.ipEquipoUsuario;
  }
  
  public void setIpEquipoUsuario(String ipEquipoUsuario) {
    this.ipEquipoUsuario = ipEquipoUsuario;
  }
  
  public String getNombreCortoEvento() {
    return this.nombreCortoEvento;
  }
  
  public void setNombreCortoEvento(String nombreCortoEvento) {
    this.nombreCortoEvento = nombreCortoEvento;
  }
  
  public String getReporte() {
    return this.reporte;
  }
  
  public void setReporte(String reporte) {
    this.reporte = reporte;
  }
  
  public String getIdUsuario() {
    return this.idUsuario;
  }
  
  public void setIdUsuario(String idUsuario) {
    this.idUsuario = idUsuario;
  }
  
  public String getDetalleEvento() {
    return this.detalleEvento;
  }
  
  public void setDetalleEvento(String detalleEvento) {
    this.detalleEvento = detalleEvento;
  }
  
  public String toString()
  {
    return "Bitacora{idBitacora=" + this.idBitacora + ", fecEvento=" + this.fecEvento + ", nombreAplicativo='" + this.nombreAplicativo + '\'' + ", ipAplicativo='" + this.ipAplicativo + '\'' + ", hostname='" + this.hostname + '\'' + ", ipEquipoUsuario='" + this.ipEquipoUsuario + '\'' + ", nombreCortoEvento='" + this.nombreCortoEvento + '\'' + ", reporte='" + this.reporte + '\'' + ", idUsuario='" + this.idUsuario + '\'' + ", detalleEvento='" + this.detalleEvento + '\'' + '}';
  }
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\model\Bitacora.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */