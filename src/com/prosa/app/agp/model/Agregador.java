package com.prosa.app.agp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.MatchPattern;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;
import org.springframework.stereotype.Component;










































@Component("Agregador")
public class Agregador
{
  private Integer oid;
  @NotNull(message="El campo agp_bco no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_bco no debe ser vacío. ")
  @Length(max=4, message="La longitud del campo agp_bco no debe ser mayor a 4 caracteres. ")
  private String banco;
  @NotNull(message="El campo agp_afiliacion no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_afiliacion no debe ser vacío. ")
  @Length(max=9, message="La longitud del campo agp_afiliacion no debe ser mayor a 9 caracteres. ")
  private Integer afiliacion;
  @NotNull(message="El campo agp_comision_cre no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_comision_cre no debe ser vacío. ")
  @Length(max=6, message="La longitud del campo agp_comision_cre no debe ser mayor a 6 caracteres. ")
  private Double comisionCre;
  @NotNull(message="El campo agp_comision_deb no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_comision_deb no debe ser vacío. ")
  @Length(max=6, message="La longitud del campo agp_comision_deb no debe ser mayor a 6 caracteres. ")
  private Double comisionDeb;
  @NotNull(message="El campo agp_sobretasa no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_sobretasa no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_sobretasa no debe ser mayor a 1 caracter. ")
  private Integer sobretasa;
  @NotNull(message="El campo agp_giro no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_giro no debe ser vacío. ")
  @Length(max=4, message="La longitud del campo agp_giro no debe ser mayor a 4 caracteres. ")
  private String giro;
  @NotNull(message="El campo agp_nombre no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_nombre no debe ser vacío. ")
  @Length(max=22, message="La longitud del campo agp_nombre no debe ser mayor a 22 caracteres. ")
  private String nombre;
  @NotNull(message="El campo agp_responsable no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_responsable no debe ser vacío. ")
  @Length(max=32, message="La longitud del campo agp_responsable no debe ser mayor a 32 caracteres. ")
  private String responsable;
  @NotNull(message="El campo agp_domicilio no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_domicilio no debe ser vacío. ")
  @Length(max=32, message="La longitud del campo agp_domicilio no debe ser mayor a 32 caracteres. ")
  private String domicilio;
  private Date fecAlta;
  private Date fecModifica;
  private Date fecBloqueo;
  @NotNull(message="El campo agp_telefono no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_telefono no debe ser vacío. ")
  @Length(max=10, message="La longitud del campo agp_telefono no debe ser mayor a 10 caracteres. ")
  private String telefono;
  @NotNull(message="El campo agp_tipo_cre no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_tipo_cre no debe ser vacío. ")
  @Length(max=4, message="La longitud del campo agp_tipo_cre no debe ser mayor a 4 caracteres. ")
  private Integer tipoCre;
  @NotNull(message="El campo agp_tipo_deb no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_tipo_deb no debe ser vacío. ")
  @Length(max=4, message="La longitud del campo agp_tipo_deb no debe ser mayor a 4 caracteres. ")
  private Integer tipoDeb;
  @NotNull(message="El campo agp_tipo_sbt no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_tipo_sbt no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_tipo_sbt no debe ser mayor a 1 caracter. ")
  private Integer tipoSbt;
  @NotNull(message="El campo agp_estatus no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_estatus no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_estatus no debe ser mayor a 1 caracter. ")
  private Integer estatus;
  private String obs;
  private String url;
  @MatchPattern(pattern={"^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}$"}, message="El correo electrónico no tiene un formato o valor válido")
  private String email;
  @NotNull(message="El campo agp_usuario no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_usuario no debe ser vacío. ")
  @Length(max=8, message="La longitud del campo agp_usuario no debe ser mayor a 8 caracteres. ")
  private String usuario;
  @NotNull(message="El campo agp_tipo_per no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_tipo_per no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_tipo_per no debe ser mayor a 1 caracter. ")
  private String tipoPersona;
  @NotNull(message="El campo agp_razon no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_razon no debe ser vacío. ")
  @Length(max=30, message="La longitud del campo agp_razon no debe ser mayor a 30 caracteres. ")
  private String razon;
  @NotNull(message="El campo agp_tipo_prod no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_tipo_prod no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_tipo_prod no debe ser mayor a 1 caracter. ")
  private String tipoProd;
  @NotNull(message="El campo agp_rep_leg1 no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_rep_leg1 no debe ser vacío. ")
  @Length(max=32, message="La longitud del campo agp_rep_leg1 no debe ser mayor a 32 caracteres. ")
  private String repLeg1;
  @Length(max=32, message="La longitud del campo agp_rep_leg2 no debe ser mayor a 32 caracteres. ")
  private String repLeg2;
  @MatchPattern(pattern={"^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}$"}, message="El correo electrónico no tiene un formato o valor válido")
  private String emailRepLeg1;
  @MatchPattern(pattern={"^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\\.[a-zA-Z]{2,4}$"}, message="El correo electrónico no tiene un formato o valor válido")
  private String emailRepLeg2;
  @NotNull(message="El campo agp_facturacion no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_facturacion no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_facturacion no debe ser mayor a 1 caracter. ")
  private String facturacion;
  @NotNull(message="El campo agp_cve_cancel no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_cve_cancel no debe ser vacío. ")
  @Length(max=1, message="La longitud del campo agp_cve_cancel no debe ser mayor a 1 caracter. ")
  private Integer cveCancel;
  @NotNull(message="El campo agp_cp no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_cp no debe ser vacío. ")
  @Length(max=5, message="La longitud del campo agp_cp no debe ser mayor a 5 caracteres. ")
  private String cp;
  @NotNull(message="El campo agp_ciudad no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_ciudad no debe ser vacío. ")
  @Length(max=20, message="La longitud del campo agp_ciudad no debe ser mayor a 20 caracteres. ")
  private String ciudad;
  @Length(max=11, message="La longitud del campo agp_mc_id no debe ser mayor a 11 caracteres. ")
  private String mcId;
  @Length(max=11, message="La longitud del campo agp_limite_vta no debe ser mayor a 11 caracteres. ")
  private Integer limiteVta;
  @Length(max=4, message="La longitud del campo agp_porc_dev no debe ser mayor a 4 caracteres. ")
  private Integer porcDev;
  @Length(max=4, message="La longitud del campo agp_porc_cagr no debe ser mayor a 4 caracteres. ")
  private Integer porcCagr;
  @Length(max=4, message="La longitud del campo agp_porc_csub no debe ser mayor a 4 caracteres. ")
  private Integer porcCsub;
  @NotNull(message="El campo agp_rfc no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_rfc no debe ser vacío. ")
  @Length(min=12, max=13, message="EL RFC receptor debe tener un tamaño entre 12 y 13 caracteres")
  @MatchPattern(pattern={"^[A-Z,\\u00D1,\\u0026]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?$"}, message="El R.F.C. receptor no tiene un formato o valor válido")
  private String rfc;
  @NotNull(message="El campo agp_colonia no debe de ser nulo. ")
  @NotEmpty(message="El campo agp_colonia no debe ser vacío. ")
  @Length(max=13, message="La longitud del campo agp_colonia no debe ser mayor a 13 caracteres. ")
  private String colonia;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String status;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String mensaje;
  
  public Integer getOid()
  {
    return this.oid;
  }
  
  public void setOid(Integer oid) {
    this.oid = oid;
  }
  
  public String getBanco() {
    return this.banco;
  }
  
  public void setBanco(String banco) {
    this.banco = banco;
  }
  
  public Integer getAfiliacion() {
    return this.afiliacion;
  }
  
  public void setAfiliacion(Integer afiliacion) {
    this.afiliacion = afiliacion;
  }
  
  public Double getComisionCre() {
    return this.comisionCre;
  }
  
  public void setComisionCre(Double comisionCre) {
    this.comisionCre = comisionCre;
  }
  
  public Double getComisionDeb() {
    return this.comisionDeb;
  }
  
  public void setComisionDeb(Double comisionDeb) {
    this.comisionDeb = comisionDeb;
  }
  
  public Integer getSobretasa() {
    return this.sobretasa;
  }
  
  public void setSobretasa(Integer sobretasa) {
    this.sobretasa = sobretasa;
  }
  
  public String getGiro() {
    return this.giro;
  }
  
  public void setGiro(String giro) {
    this.giro = giro;
  }
  
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  public String getResponsable() {
    return this.responsable;
  }
  
  public void setResponsable(String responsable) {
    this.responsable = responsable;
  }
  
  public String getDomicilio() {
    return this.domicilio;
  }
  
  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }
  
  public Date getFecAlta() {
    return this.fecAlta;
  }
  
  public void setFecAlta(Date fecAlta) {
    this.fecAlta = fecAlta;
  }
  
  public Date getFecModifica() {
    return this.fecModifica;
  }
  
  public void setFecModifica(Date fecModifica) {
    this.fecModifica = fecModifica;
  }
  
  public Date getFecBloqueo() {
    return this.fecBloqueo;
  }
  
  public void setFecBloqueo(Date fecBloqueo) {
    this.fecBloqueo = fecBloqueo;
  }
  
  public String getTelefono() {
    return this.telefono;
  }
  
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
  
  public Integer getTipoCre() {
    return this.tipoCre;
  }
  
  public void setTipoCre(Integer tipoCre) {
    this.tipoCre = tipoCre;
  }
  
  public Integer getTipoDeb() {
    return this.tipoDeb;
  }
  
  public void setTipoDeb(Integer tipoDeb) {
    this.tipoDeb = tipoDeb;
  }
  
  public Integer getTipoSbt() {
    return this.tipoSbt;
  }
  
  public void setTipoSbt(Integer tipoSbt) {
    this.tipoSbt = tipoSbt;
  }
  
  public Integer getEstatus() {
    return this.estatus;
  }
  
  public void setEstatus(Integer estatus) {
    this.estatus = estatus;
  }
  
  public String getObs() {
    return this.obs;
  }
  
  public void setObs(String obs) {
    this.obs = obs;
  }
  
  public String getUrl() {
    return this.url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getUsuario() {
    return this.usuario;
  }
  
  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
  
  public String getTipoPersona() {
    return this.tipoPersona;
  }
  
  public void setTipoPersona(String tipoPersona) {
    this.tipoPersona = tipoPersona;
  }
  
  public String getRazon() {
    return this.razon;
  }
  
  public void setRazon(String razon) {
    this.razon = razon;
  }
  
  public String getTipoProd() {
    return this.tipoProd;
  }
  
  public void setTipoProd(String tipoProd) {
    this.tipoProd = tipoProd;
  }
  
  public String getRepLeg1() {
    return this.repLeg1;
  }
  
  public void setRepLeg1(String repLeg1) {
    this.repLeg1 = repLeg1;
  }
  
  public String getRepLeg2() {
    return this.repLeg2;
  }
  
  public void setRepLeg2(String repLeg2) {
    this.repLeg2 = repLeg2;
  }
  
  public String getEmailRepLeg1() {
    return this.emailRepLeg1;
  }
  
  public void setEmailRepLeg1(String emailRepLeg1) {
    this.emailRepLeg1 = emailRepLeg1;
  }
  
  public String getEmailRepLeg2() {
    return this.emailRepLeg2;
  }
  
  public void setEmailRepLeg2(String emailRepLeg2) {
    this.emailRepLeg2 = emailRepLeg2;
  }
  
  public String getFacturacion() {
    return this.facturacion;
  }
  
  public void setFacturacion(String facturacion) {
    this.facturacion = facturacion;
  }
  
  public Integer getCveCancel() {
    return this.cveCancel;
  }
  
  public void setCveCancel(Integer cveCancel) {
    this.cveCancel = cveCancel;
  }
  
  public String getCp() {
    return this.cp;
  }
  
  public void setCp(String cp) {
    this.cp = cp;
  }
  
  public String getCiudad() {
    return this.ciudad;
  }
  
  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }
  
  public String getMcId() {
    return this.mcId;
  }
  
  public void setMcId(String mcId) {
    this.mcId = mcId;
  }
  
  public Integer getLimiteVta() {
    return this.limiteVta;
  }
  
  public void setLimiteVta(Integer limiteVta) {
    this.limiteVta = limiteVta;
  }
  
  public Integer getPorcDev() {
    return this.porcDev;
  }
  
  public void setPorcDev(Integer porcDev) {
    this.porcDev = porcDev;
  }
  
  public Integer getPorcCagr() {
    return this.porcCagr;
  }
  
  public void setPorcCagr(Integer porcCagr) {
    this.porcCagr = porcCagr;
  }
  
  public Integer getPorcCsub() {
    return this.porcCsub;
  }
  
  public void setPorcCsub(Integer porcCsub) {
    this.porcCsub = porcCsub;
  }
  
  public String getRfc() {
    return this.rfc;
  }
  
  public void setRfc(String rfc) {
    this.rfc = rfc;
  }
  
  public String getColonia() {
    return this.colonia;
  }
  
  public void setColonia(String colonia) {
    this.colonia = colonia;
  }
  
  public String getStatus() {
    return this.status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getMensaje() {
    return this.mensaje;
  }
  
  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\model\Agregador.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */