package com.prosa.app.agp.dao;

import com.prosa.app.agp.model.Bitacora;
import java.sql.CallableStatement;
import java.sql.Connection;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component("bitacoraDAO")
public class BitacoraDAO
{
  @Autowired
  DataSource subAfiliados;
  
  public void inserta(Bitacora bitacora)
    throws Exception
  {
    try
    {
      Connection con = this.subAfiliados.getConnection();Throwable localThrowable6 = null;
      try { String query = "{ call\t spInsBitacora(?,?,?,?,?,?,?,?)}";
        CallableStatement call = con.prepareCall("{ call\t spInsBitacora(?,?,?,?,?,?,?,?)}");Throwable localThrowable7 = null;
        try { call.setString(1, bitacora.getNombreAplicativo());
          call.setString(2, bitacora.getIpAplicativo());
          call.setString(3, bitacora.getHostname());
          call.setString(4, bitacora.getIpEquipoUsuario());
          call.setString(5, bitacora.getNombreCortoEvento());
          call.setString(6, bitacora.getReporte());
          call.setString(7, bitacora.getIdUsuario());
          call.setString(8, bitacora.getDetalleEvento());
          call.execute();
          log.info("Bitacora registrada exitosamente");
        }
        catch (Throwable localThrowable1)
        {
          localThrowable7 = localThrowable1;throw localThrowable1;
        }
        finally {}
      }
      catch (Throwable localThrowable4)
      {
        localThrowable6 = localThrowable4;throw localThrowable4;





      }
      finally
      {





        if (con != null) if (localThrowable6 != null) try { con.close(); } catch (Throwable localThrowable5) { localThrowable6.addSuppressed(localThrowable5); } else con.close();
      }
    } catch (Exception e) { throw new Exception("Error al insertar bitacora [{}]", e);
    }
  }
  

  private static final Logger log = Logger.getLogger(AgregadorDAO.class);
  private static final String spInsBitacora = "spInsBitacora(?,?,?,?,?,?,?,?)";
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\dao\BitacoraDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */