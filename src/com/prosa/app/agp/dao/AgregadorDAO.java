package com.prosa.app.agp.dao;

import com.prosa.app.agp.model.Agregador;
import com.prosa.app.agp.model.AgregadorResponse;
import com.prosa.app.agp.model.AgregadorResponseList;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;













@Component("agregadorDAO")
public class AgregadorDAO
{
  @Autowired
  DataSource subAfiliados;
  
  public AgregadorDAO() { this.mensaje = ""; }
  
  public void inserta(Agregador agregador) {
    try {
      Connection con = this.subAfiliados.getConnection();Throwable localThrowable6 = null;
      try { String query = "{ call\t spInsAgregador(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement call = con.prepareCall("{ call\t spInsAgregador(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");Throwable localThrowable7 = null;
        try { call.setString(1, agregador.getBanco());
          call.setDouble(2, agregador.getComisionCre().doubleValue());
          call.setDouble(3, agregador.getComisionDeb().doubleValue());
          call.setDouble(4, agregador.getSobretasa().intValue());
          call.setString(5, agregador.getGiro());
          call.setString(6, agregador.getNombre());
          call.setString(7, agregador.getResponsable());
          call.setString(8, agregador.getDomicilio());
          call.setString(9, agregador.getTelefono());
          call.setInt(10, agregador.getTipoCre().intValue());
          call.setInt(11, agregador.getTipoDeb().intValue());
          call.setInt(12, agregador.getTipoSbt().intValue());
          call.setInt(13, agregador.getEstatus().intValue());
          call.setString(14, agregador.getObs());
          call.setString(15, agregador.getUrl());
          call.setString(16, agregador.getEmail());
          call.setString(17, agregador.getUsuario());
          call.setString(18, agregador.getTipoPersona());
          call.setString(19, agregador.getRazon());
          call.setString(20, agregador.getTipoProd());
          call.setString(21, agregador.getRepLeg1());
          call.setString(22, agregador.getRepLeg2());
          call.setString(23, agregador.getEmailRepLeg1());
          call.setString(24, agregador.getEmailRepLeg2());
          call.setString(25, agregador.getFacturacion());
          call.setInt(26, agregador.getCveCancel().intValue());
          call.setString(27, agregador.getCp());
          call.setString(28, agregador.getCiudad());
          call.setString(29, agregador.getMcId());
          call.setInt(30, agregador.getLimiteVta().intValue());
          call.setInt(31, agregador.getPorcDev().intValue());
          call.setInt(32, agregador.getPorcCagr().intValue());
          call.setInt(33, agregador.getPorcCsub().intValue());
          call.setString(34, agregador.getRfc());
          call.setString(35, agregador.getColonia());
          call.registerOutParameter(36, 4);
          call.registerOutParameter(37, 12);
          call.registerOutParameter(38, 12);
          call.registerOutParameter(39, 4);
          call.registerOutParameter(40, 91);
          call.execute();
          agregador.setOid(Integer.valueOf(call.getInt(36)));
          this.status = call.getString(37);
          this.mensaje = call.getString(38);
          this.afiliacion = Integer.valueOf(call.getInt(39));
          this.fec_alta = call.getDate(40);
          log.info("Agregador insertado correctamente");
          agregador.setMensaje(this.mensaje);
          agregador.setStatus(this.status);
          agregador.setAfiliacion(this.afiliacion);
          agregador.setFecAlta(this.fec_alta);
          agregador.setFecModifica(this.fec_alta);
        }
        catch (Throwable localThrowable1)
        {
          localThrowable7 = localThrowable1;throw localThrowable1;
        }
        finally {}
      }
      catch (Throwable localThrowable4)
      {
        localThrowable6 = localThrowable4;throw localThrowable4;


























      }
      finally
      {


























        if (con != null) if (localThrowable6 != null) try { con.close(); } catch (Throwable localThrowable5) { localThrowable6.addSuppressed(localThrowable5); } else con.close();
      }
    } catch (Exception e) { this.status = "ERROR";
      this.mensaje = e.getMessage();
      agregador.setStatus(this.status);
      agregador.setMensaje(this.mensaje);
      log.error("Error al insertar agregador [{}]", e);
    }
  }
  
  public void actualiza(Agregador agregador) {
    try { Connection con = this.subAfiliados.getConnection();Throwable localThrowable6 = null;
      try { String query = "{ call\t spUpdAgregador(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        CallableStatement call = con.prepareCall("{ call\t spUpdAgregador(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");Throwable localThrowable7 = null;
        try { call.setString(1, agregador.getBanco());
          call.setInt(2, agregador.getAfiliacion().intValue());
          call.setDouble(3, agregador.getComisionCre().doubleValue());
          call.setDouble(4, agregador.getComisionDeb().doubleValue());
          call.setDouble(5, agregador.getSobretasa().intValue());
          call.setString(6, agregador.getGiro());
          call.setString(7, agregador.getNombre());
          call.setString(8, agregador.getResponsable());
          call.setString(9, agregador.getDomicilio());
          call.setString(10, agregador.getTelefono());
          call.setInt(11, agregador.getTipoCre().intValue());
          call.setInt(12, agregador.getTipoDeb().intValue());
          call.setInt(13, agregador.getTipoSbt().intValue());
          call.setInt(14, agregador.getEstatus().intValue());
          call.setString(15, agregador.getObs());
          call.setString(16, agregador.getUrl());
          call.setString(17, agregador.getEmail());
          call.setString(18, agregador.getUsuario());
          call.setString(19, agregador.getTipoPersona());
          call.setString(20, agregador.getRazon());
          call.setString(21, agregador.getTipoProd());
          call.setString(22, agregador.getRepLeg1());
          call.setString(23, agregador.getRepLeg2());
          call.setString(24, agregador.getEmailRepLeg1());
          call.setString(25, agregador.getEmailRepLeg2());
          call.setString(26, agregador.getFacturacion());
          call.setInt(27, agregador.getCveCancel().intValue());
          call.setString(28, agregador.getCp());
          call.setString(29, agregador.getCiudad());
          call.setString(30, agregador.getMcId());
          call.setInt(31, agregador.getLimiteVta().intValue());
          call.setInt(32, agregador.getPorcDev().intValue());
          call.setInt(33, agregador.getPorcCagr().intValue());
          call.setInt(34, agregador.getPorcCsub().intValue());
          call.setString(35, agregador.getRfc());
          call.setString(36, agregador.getColonia());
          call.registerOutParameter(37, 12);
          call.registerOutParameter(38, 12);
          call.registerOutParameter(39, 91);
          call.registerOutParameter(40, 91);
          call.registerOutParameter(41, 4);
          call.execute();
          this.status = call.getString(37);
          this.mensaje = call.getString(38);
          this.fec_modifica = call.getDate(39);
          this.fec_alta = call.getDate(40);
          this.oid = Integer.valueOf(call.getInt(41));
          agregador.setMensaje(this.mensaje);
          agregador.setStatus(this.status);
          agregador.setFecModifica(this.fec_modifica);
          agregador.setFecAlta(this.fec_alta);
          agregador.setOid(this.oid);
          log.info("Agregador actualizado correctamente");
        }
        catch (Throwable localThrowable1)
        {
          localThrowable7 = localThrowable1;throw localThrowable1;
        }
        finally {}
      }
      catch (Throwable localThrowable4)
      {
        localThrowable6 = localThrowable4;throw localThrowable4;



























      }
      finally
      {


























        if (con != null) if (localThrowable6 != null) try { con.close(); } catch (Throwable localThrowable5) { localThrowable6.addSuppressed(localThrowable5); } else con.close();
      }
    } catch (Exception e) { this.status = "ERROR";
      this.mensaje = e.getMessage();
      log.error(e);
    }
  }
  
  public AgregadorResponseList listaAgregadores(String nombre, Integer afiliacion, String rfc) throws Exception {
    List<AgregadorResponse> lstCuentas = new ArrayList();
    AgregadorResponseList cuenta = new AgregadorResponseList();
    try { Connection con = this.subAfiliados.getConnection();Throwable localThrowable9 = null;
      try { con.setAutoCommit(false);
        String query = "begin ? := spSelAgregador(?,?,?); end;";
        CallableStatement call = con.prepareCall("begin ? := spSelAgregador(?,?,?); end;");Throwable localThrowable10 = null;
        try { call.registerOutParameter(1, -10);
          call.setString(2, nombre);
          call.setInt(3, afiliacion.intValue());
          call.setString(4, rfc);
          call.execute();
          try { ResultSet rs = (ResultSet)call.getObject(1);Throwable localThrowable11 = null;
            try { while (rs.next()) {
                AgregadorResponse agregador = new AgregadorResponse();
                agregador.setOid(Integer.valueOf(rs.getInt("OID")));
                agregador.setBanco(rs.getString("AGP_BCO"));
                agregador.setAfiliacion(Integer.valueOf(rs.getInt("AGP_AFILIACION")));
                agregador.setComisionCre(Double.valueOf(rs.getDouble("AGP_COMISION_CRE")));
                agregador.setComisionDeb(Double.valueOf(rs.getDouble("AGP_COMISION_DEB")));
                agregador.setSobretasa(Integer.valueOf(rs.getInt("AGP_SOBRETASA")));
                agregador.setGiro(rs.getString("AGP_GIRO"));
                agregador.setNombre(rs.getString("AGP_NOMBRE"));
                agregador.setResponsable(rs.getString("AGP_RESPONSABLE"));
                agregador.setDomicilio(rs.getString("AGP_DOMICILIO"));
                agregador.setFecAlta(rs.getDate("AGP_FEC_ALTA"));
                agregador.setFecModifica(rs.getDate("AGP_FEC_MOD"));
                agregador.setFecBloqueo(rs.getDate("AGP_FEC_BLOQUEO"));
                agregador.setTelefono(rs.getString("AGP_TELEFONO"));
                agregador.setTipoCre(Integer.valueOf(rs.getInt("AGP_TIPO_CRE")));
                agregador.setTipoDeb(Integer.valueOf(rs.getInt("AGP_TIPO_DEB")));
                agregador.setTipoSbt(Integer.valueOf(rs.getInt("AGP_TIPO_SBT")));
                agregador.setEstatus(Integer.valueOf(rs.getInt("AGP_ESTATUS")));
                agregador.setObs(rs.getString("AGP_OBS"));
                agregador.setUrl(rs.getString("AGP_URL"));
                agregador.setEmail(rs.getString("AGP_EMAIL"));
                agregador.setUsuario(rs.getString("AGP_USUARIO"));
                agregador.setTipoPersona(rs.getString("AGP_TIPO_PER"));
                agregador.setRazon(rs.getString("AGP_RAZON"));
                agregador.setTipoProd(rs.getString("AGP_TIPO_PROD"));
                agregador.setRepLeg1(rs.getString("AGP_REP_LEG1"));
                agregador.setRepLeg2(rs.getString("AGP_REP_LEG2"));
                agregador.setEmailRepLeg1(rs.getString("AGP_EMAIL_RL1"));
                agregador.setEmailRepLeg2(rs.getString("AGP_EMAIL_RL2"));
                agregador.setFacturacion(rs.getString("AGP_FACTURACION"));
                agregador.setCveCancel(Integer.valueOf(rs.getInt("AGP_CVE_CANCEL")));
                agregador.setCp(rs.getString("AGP_CP"));
                agregador.setCiudad(rs.getString("AGP_CIUDAD"));
                agregador.setMcId(rs.getString("AGP_MC_ID"));
                agregador.setLimiteVta(Integer.valueOf(rs.getInt("AGP_LIMITE_VTA")));
                agregador.setPorcDev(Integer.valueOf(rs.getInt("AGP_PORC_DEV")));
                agregador.setPorcCagr(Integer.valueOf(rs.getInt("AGP_PORC_CAGR")));
                agregador.setPorcCsub(Integer.valueOf(rs.getInt("AGP_PORC_CSUB")));
                agregador.setRfc(rs.getString("AGP_RFC"));
                agregador.setColonia(rs.getString("AGP_COLONIA"));
                lstCuentas.add(agregador);
              }
              if (lstCuentas.size() > 0) {
                cuenta.setMensaje("Consulta exitosa");
                cuenta.setStatus("OK");
                cuenta.setListAgregadores(lstCuentas);
              }
              else {
                cuenta.setMensaje("Agregador no encontrado");
                cuenta.setStatus("ERROR");
              }
            }
            catch (Throwable localThrowable1)
            {
              localThrowable11 = localThrowable1;throw localThrowable1;

















            }
            finally {}

















          }
          catch (Exception e)
          {
















            this.status = "ERROR";
            this.mensaje = e.getMessage();
            throw new Exception("Error al consultar la tabla AGP_AGREGADORES");
          }
        }
        catch (Throwable localThrowable4)
        {
          localThrowable10 = localThrowable4;throw localThrowable4;
        }
        finally {}
      }
      catch (Throwable localThrowable7)
      {
        localThrowable9 = localThrowable7;throw localThrowable7;

































      }
      finally
      {

































        if (con != null) if (localThrowable9 != null) try { con.close(); } catch (Throwable localThrowable8) { localThrowable9.addSuppressed(localThrowable8); } else con.close();
      }
    } catch (Exception e2) { this.status = "ERROR";
      this.mensaje = e2.getMessage();
      throw new Exception("Error al consultar la tabla AGP_AGREGADORES");
    }
    return cuenta;
  }
  
  public Agregador cancela(String banco, Integer afiliacion, Integer agregadorP) {
    Agregador agregador = new Agregador();
    try { Connection con = this.subAfiliados.getConnection();Throwable localThrowable6 = null;
      try { String query = "{ call\t spCancelaAgregador(?,?,?,?,?)}";
        CallableStatement call = con.prepareCall("{ call\t spCancelaAgregador(?,?,?,?,?)}");Throwable localThrowable7 = null;
        try { call.setString(1, banco);
          call.setInt(2, afiliacion.intValue());
          call.setInt(3, agregadorP.intValue());
          call.registerOutParameter(4, 12);
          call.registerOutParameter(5, 12);
          call.execute();
          this.status = call.getString(4);
          agregador.setMensaje(this.mensaje = call.getString(5));
          agregador.setStatus(this.status);
          log.info("Cancelación exitosa");
        }
        catch (Throwable localThrowable1)
        {
          localThrowable7 = localThrowable1;throw localThrowable1;
        }
        finally {}
      }
      catch (Throwable localThrowable4)
      {
        localThrowable6 = localThrowable4;throw localThrowable4;





      }
      finally
      {





        if (con != null) if (localThrowable6 != null) try { con.close(); } catch (Throwable localThrowable5) { localThrowable6.addSuppressed(localThrowable5); } else con.close();
      }
    } catch (Exception e) { this.status = "ERROR";
      this.mensaje = e.getMessage();
      log.error(e);
    }
    return agregador;
  }
  
  public Agregador bloquea(String banco, Integer afiliacion, Integer agregadorP) {
    Agregador agregador = new Agregador();
    try { Connection con = this.subAfiliados.getConnection();Throwable localThrowable6 = null;
      try { String query = "{ call\t spBloqueaAgregador(?,?,?,?,?)}";
        CallableStatement call = con.prepareCall("{ call\t spBloqueaAgregador(?,?,?,?,?)}");Throwable localThrowable7 = null;
        try { call.setString(1, banco);
          call.setInt(2, afiliacion.intValue());
          call.setInt(3, agregadorP.intValue());
          call.registerOutParameter(4, 12);
          call.registerOutParameter(5, 12);
          call.execute();
          this.status = call.getString(4);
          agregador.setMensaje(this.mensaje = call.getString(5));
          agregador.setStatus(this.status);
          log.info("Bloqueo exitoso");
        }
        catch (Throwable localThrowable1)
        {
          localThrowable7 = localThrowable1;throw localThrowable1;
        }
        finally {}
      }
      catch (Throwable localThrowable4)
      {
        localThrowable6 = localThrowable4;throw localThrowable4;





      }
      finally
      {





        if (con != null) if (localThrowable6 != null) try { con.close(); } catch (Throwable localThrowable5) { localThrowable6.addSuppressed(localThrowable5); } else con.close();
      }
    } catch (Exception e) { this.status = "ERROR";
      this.mensaje = e.getMessage();
      log.error(e);
    }
    return agregador;
  }
  

  private static final Logger log = Logger.getLogger(AgregadorDAO.class);
  private static final String spInsAgregador = "spInsAgregador(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  private static final String spUpdAgregador = "spUpdAgregador(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  private static final String spCancelaAgregador = "spCancelaAgregador(?,?,?,?,?)";
  private static final String spBloqueaAgregador = "spBloqueaAgregador(?,?,?,?,?)";
  public Integer oid;
  public String status;
  public String mensaje;
  public Integer afiliacion;
  public Date fec_alta;
  public Date fec_modifica;
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\dao\AgregadorDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */