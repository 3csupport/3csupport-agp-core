package com.prosa.app.agp.seguridad;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQueryBuilder;







public class GenericLDAPDAO
{
  private LdapTemplate ldapTemplate;
  private LdapTemplate prefixLdapTemplate;
  
  public LdapTemplate getLdapTemplate()
  {
    return this.ldapTemplate;
  }
  
  public LdapTemplate getPrefixLdapTemplate() {
    return this.prefixLdapTemplate;
  }
  
  @Autowired
  @Qualifier("ldapTemplateProsa")
  public void setLdapTemplate(LdapTemplate ldapTemplate) {
    this.ldapTemplate = ldapTemplate;
  }
  
  @Autowired
  @Qualifier("prefixLdapTemplateProsa")
  public void setPrefixLdapTemplate(LdapTemplate ldapTemplate) {
    this.prefixLdapTemplate = ldapTemplate;
  }
  
  public LdapQueryBuilder crearLdapQuery() {
    return LdapQueryBuilder.query();
  }
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\seguridad\GenericLDAPDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */