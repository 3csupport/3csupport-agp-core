package com.prosa.app.agp.seguridad.impl;

import com.prosa.app.agp.seguridad.*;
import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.ldap.core.*;
import javax.naming.directory.*;
import javax.naming.*;
import org.springframework.ldap.query.*;
import org.slf4j.*;

@Repository("prosaSeguridadDAO")
public class ProsaSeguridadDAOImpl extends GenericLDAPDAO implements ProsaSeguridadDAO
{
    private static final Logger LOGGER;
    @Value("${ldap.user.nombreDeUsuario}")
    private String nombreDeUsuario;
    
    public boolean existeUsuario(final String usuario) throws Exception {
        try {
            ProsaSeguridadDAOImpl.LOGGER.info(" nombreDeUsuario {}", (Object)usuario);
            return this.getLdapTemplate().search((LdapQuery)this.crearLdapQuery().attributes(new String[] { this.nombreDeUsuario }).where(this.nombreDeUsuario).is(usuario), (AttributesMapper)new AttributesMapper<String>() {
                public String mapFromAttributes(final Attributes attributes) throws NamingException {
                    return (String)attributes.get(ProsaSeguridadDAOImpl.this.nombreDeUsuario).get();
                }
            }).size() > 0;
        }
        catch (Exception exception) {
            ProsaSeguridadDAOImpl.LOGGER.error("Error al buscar el usuario: {}, la razon fue: {}", (Object)usuario, (Object)exception.getMessage());
            ProsaSeguridadDAOImpl.LOGGER.error(" Error {}", (Throwable)exception);
            throw new Exception("ERROR AL BUSCAR USUARIO", exception);
        }
    }
    
    static {
        LOGGER = LoggerFactory.getLogger((Class)ProsaSeguridadDAOImpl.class);
    }
}
