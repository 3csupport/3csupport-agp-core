package com.prosa.app.agp.ws;

import com.prosa.app.agp.controller.AgregadorController;
import com.prosa.app.agp.controller.BitacoraController;
import com.prosa.app.agp.model.Agregador;
import com.prosa.app.agp.model.AgregadorResponseList;
import com.prosa.app.agp.model.Bitacora;
import com.prosa.app.agp.service.UsuarioLdapService;
import com.prosa.app.agp.utils.JAXRSResourceSpringSupport;
import com.prosa.app.agp.utils.LogClass;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

@Path("/")
public class SubAfiliadosView
  extends JAXRSResourceSpringSupport
{
  private static final Logger log = Logger.getLogger(SubAfiliadosView.class);
  private static final String separator = "|";
  String cadena;
  @Context
  private HttpServletRequest hsrAlta;
  @Context
  private HttpServletRequest hsrModifica;
  @Context
  private HttpServletRequest hsrConsulta;
  @Context
  private HttpServletRequest hsrCancela;
  @Context
  private HttpServletRequest hsrBloquea;
  
  @POST
  @Path("SubAfiliados/AltaAgregador")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response addAgregador(@RequestBody Agregador agregadorDTO, @HeaderParam("authorization") String authString)
  {
    try
    {
      log.info("Dato autorizacion: " + authString);
      if (((UsuarioLdapService)getBean(UsuarioLdapService.class)).existeUsuario(authString))
      {
        log.info("Inicia el servicio REST AltaAgregador");
        ((AgregadorController)getBean(AgregadorController.class)).addAgregador(agregadorDTO);
        log.info("Alta desde el Rest");
        if (!agregadorDTO.getStatus().equals("ERROR"))
        {
          log.info("Inicia registro en bitacora");
          Bitacora bitacora = ((BitacoraController)getBean(BitacoraController.class)).getBitacoraInserta(this.hsrAlta);
          ((BitacoraController)getBean(BitacoraController.class)).addBitacora(bitacora);
          this.cadena = ((BitacoraController)getBean(BitacoraController.class)).getCadena(bitacora);
          ((LogClass)getBean(LogClass.class)).getLogger().info(this.cadena);
        }
        log.info(agregadorDTO.getStatus());
        log.info(agregadorDTO.getMensaje());
        return Response.status(Response.Status.OK).entity(agregadorDTO).build();
      }
      return Response.status(Response.Status.BAD_REQUEST).entity("Usuario no autenticado").build();
    }
    catch (Exception e)
    {
      log.error(e.getMessage(), e);
      return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
  }
  
  @POST
  @Path("SubAfiliados/ModificaAgregador")
  @Consumes({"application/json"})
  @Produces({"application/json"})
  public Response updAgregador(@RequestBody Agregador agregadorDTO, @HeaderParam("authorization") String authString)
  {
    try
    {
      if (((UsuarioLdapService)getBean(UsuarioLdapService.class)).existeUsuario(authString))
      {
        log.info("Inicia el servicio REST ModificaAgregador");
        ((AgregadorController)getBean(AgregadorController.class)).updAgregador(agregadorDTO);
        log.info("Actualizaci��n desde el Rest");
        if (!agregadorDTO.getStatus().equals("ERROR"))
        {
          log.info("Inicia registro en bitacora");
          Bitacora bitacora = ((BitacoraController)getBean(BitacoraController.class)).getBitacoraActualiza(this.hsrModifica);
          ((BitacoraController)getBean(BitacoraController.class)).addBitacora(bitacora);
          this.cadena = ((BitacoraController)getBean(BitacoraController.class)).getCadena(bitacora);
          ((LogClass)getBean(LogClass.class)).getLogger().info(this.cadena);
        }
        log.info(agregadorDTO.getStatus());
        log.info(agregadorDTO.getMensaje());
        return Response.status(Response.Status.OK).entity(agregadorDTO).build();
      }
      return Response.status(Response.Status.BAD_REQUEST).entity("Usuario no autenticado").build();
    }
    catch (Exception e)
    {
      log.error(e.getMessage(), e);
      return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
  }
  
  @GET
  @Path("SubAfiliados/ConsultaAgregador/{nombre}/{afiliacion}/{rfc}")
  @Produces({"application/json"})
  public Response selAgregador(@PathParam("nombre") String nombre, @PathParam("afiliacion") Integer afiliacion, @PathParam("rfc") String rfc, @HeaderParam("authorization") String authString)
  {
    try
    {
      if (((UsuarioLdapService)getBean(UsuarioLdapService.class)).existeUsuario(authString))
      {
        log.info("Inicia el servicio REST ConsultaAgregador");
        AgregadorResponseList agregadores = ((AgregadorController)getBean(AgregadorController.class)).selAgregador(nombre, afiliacion, rfc);
        log.info("Consulta desde el Rest");
        if (!agregadores.getStatus().equals("ERROR"))
        {
          log.info("Inicia registro en bitacora");
          Bitacora bitacora = ((BitacoraController)getBean(BitacoraController.class)).getBitacoraConsulta(this.hsrConsulta);
          ((BitacoraController)getBean(BitacoraController.class)).addBitacora(bitacora);
          this.cadena = ((BitacoraController)getBean(BitacoraController.class)).getCadena(bitacora);
          ((LogClass)getBean(LogClass.class)).getLogger().info(this.cadena);
        }
        log.info(agregadores.getStatus());
        log.info(agregadores.getMensaje());
        return Response.status(Response.Status.OK).entity(agregadores).build();
      }
      return Response.status(Response.Status.BAD_REQUEST).entity("Usuario no autenticado").build();
    }
    catch (Exception e)
    {
      log.error(e.getMessage(), e);
      return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
  }
  
  @GET
  @Path("SubAfiliados/CancelaAgregador/{banco}/{afiliacion}/{agregador}")
  @Produces({"application/json"})
  public Response cancelaAgregador(@PathParam("banco") String banco, @PathParam("afiliacion") Integer afiliacion, @PathParam("agregador") Integer agregador, @HeaderParam("authorization") String authString)
  {
    try
    {
      if (((UsuarioLdapService)getBean(UsuarioLdapService.class)).existeUsuario(authString))
      {
        log.info("Inicia el servicio REST CancelaAgregador");
        Agregador agregadores = ((AgregadorController)getBean(AgregadorController.class)).cancelaAgregador(banco, afiliacion, agregador);
        log.info("Cancelaci��n desde el Rest");
        if (!agregadores.getStatus().equals("ERROR"))
        {
          log.info("Inicia registro en bitacora");
          Bitacora bitacora = ((BitacoraController)getBean(BitacoraController.class)).getBitacoraCancela(this.hsrCancela);
          ((BitacoraController)getBean(BitacoraController.class)).addBitacora(bitacora);
          this.cadena = ((BitacoraController)getBean(BitacoraController.class)).getCadena(bitacora);
          ((LogClass)getBean(LogClass.class)).getLogger().info(this.cadena);
        }
        log.info(agregadores.getStatus());
        log.info(agregadores.getMensaje());
        JSONObject json = new JSONObject();
        json.put("banco", banco);
        json.put("afiliacion", afiliacion);
        json.put("agregador", agregador);
        json.put("status", agregadores.getStatus());
        json.put("mensaje", agregadores.getMensaje());
        return Response.status(Response.Status.OK).entity(json.toString()).build();
      }
      return Response.status(Response.Status.BAD_REQUEST).entity("Usuario no autenticado").build();
    }
    catch (Exception e)
    {
      log.error(e.getMessage(), e);
      return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
  }
  
  @GET
  @Path("SubAfiliados/BloqueoAgregador/{banco}/{afiliacion}/{agregador}")
  @Produces({"application/json"})
  public Response bloqueoAgregador(@PathParam("banco") String banco, @PathParam("afiliacion") Integer afiliacion, @PathParam("agregador") Integer agregador, @HeaderParam("authorization") String authString)
  {
    try
    {
      if (((UsuarioLdapService)getBean(UsuarioLdapService.class)).existeUsuario(authString))
      {
        log.info("Inicia el servicio REST BloqueoAgregador");
        Agregador agregadores = ((AgregadorController)getBean(AgregadorController.class)).bloqueaAgregador(banco, afiliacion, agregador);
        log.info("Bloqueo desde el Rest");
        if (!agregadores.getStatus().equals("ERROR"))
        {
          log.info("Inicia registro en bitacora");
          Bitacora bitacora = ((BitacoraController)getBean(BitacoraController.class)).getBitacoraBloqueo(this.hsrBloquea);
          ((BitacoraController)getBean(BitacoraController.class)).addBitacora(bitacora);
          this.cadena = ((BitacoraController)getBean(BitacoraController.class)).getCadena(bitacora);
          ((LogClass)getBean(LogClass.class)).getLogger().info(this.cadena);
        }
        log.info(agregadores.getStatus());
        log.info(agregadores.getMensaje());
        JSONObject json = new JSONObject();
        json.put("banco", banco);
        json.put("afiliacion", afiliacion);
        json.put("agregador", agregador);
        json.put("status", agregadores.getStatus());
        json.put("mensaje", agregadores.getMensaje());
        return Response.status(Response.Status.OK).entity(json.toString()).build();
      }
      return Response.status(Response.Status.BAD_REQUEST).entity("Usuario no autenticado").build();
    }
    catch (Exception e)
    {
      log.error(e.getMessage(), e);
      return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
  }
}
