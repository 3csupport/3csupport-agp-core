package com.prosa.app.agp.utils;

import java.util.Properties;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;





public class JAXRSResourceSpringSupport
{
  @Context
  private ServletContext servletContext;
  
  public <T> T getBean(Class<T> beanClass)
  {
    return (T)WebApplicationContextUtils.getWebApplicationContext(this.servletContext).getBean(beanClass);
  }
  
  public String getProperty(String props, String key) {
    return ((Properties)WebApplicationContextUtils.getWebApplicationContext(this.servletContext).getBean(props, Properties.class)).getProperty(key);
  }
}