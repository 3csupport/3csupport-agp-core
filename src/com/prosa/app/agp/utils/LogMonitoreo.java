package com.prosa.app.agp.utils;

import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.Logger;

public class LogMonitoreo {
	Timer timer;
	String pass;
	private static final Logger log = Logger.getLogger(LogMonitoreo.class);

	public LogMonitoreo(Integer tiempo, String pass) {
		this.timer = new Timer();

		this.pass = pass;

		Integer periodo = Integer.valueOf(tiempo.intValue() * 1000);

		this.timer.schedule(new LogClassMonitor(), periodo.intValue(), periodo.intValue());
	}

	class LogClassMonitor extends TimerTask {
		int limite = 0;
		TimerTask task;

		LogClassMonitor() {
		}

		public void run() {
			if (LogMonitoreo.this.pass.equals("Armala2018-")) {
				LogMonitoreo.log.info("La aplicacion esta activa");
			} else {
				this.limite += 1;
				if (this.limite <= 20) {
					LogMonitoreo.log.info("La aplicacion esta activa");
				} else {
					LogMonitoreo.this.timer.cancel();
				}
			}
		}
	}
}
