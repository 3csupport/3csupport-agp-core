package com.prosa.app.agp.utils;

import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

import org.springframework.stereotype.Component;
import org.slf4j.Logger;

@Component
public class LogClass extends org.apache.log4j.Logger {
	
	protected LogClass(String fileName) {
		
		super(fileName);
		// Incio genera archivo log
	}

	private String fileName;
	
	public void initializationFlag() {
		
	}
	
	public Logger getLogger() {
		
		return null;
	}
	
	public boolean exists(Path path, LinkOption file) {
		
		return false;
	}
	
	public void createFile(Path path, FileAttribute attribute){
		
	}
	
}