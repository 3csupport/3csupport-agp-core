package com.prosa.app.agp.service.impl;

import com.prosa.app.agp.seguridad.ProsaSeguridadDAO;
import com.prosa.app.agp.service.UsuarioLdapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("usuarioLdapService")
public class UsuarioLdapServiceImpl
  implements UsuarioLdapService
{
  @Autowired
  ProsaSeguridadDAO prosaSeguridadDAO;
  
  public boolean existeUsuario(String usuario)
    throws Exception
  {
    try
    {
      return (usuario != null) && (usuario != "") && (this.prosaSeguridadDAO.existeUsuario(usuario));
    }
    catch (Exception e) {
      throw new Exception("PROSASEG", e);
    }
  }
}


/* Location:              E:\Users\mfernandez\Documents\_personal\3csupport\WS\agp-core.war!\WEB-INF\classes\com\prosa\app\agp\service\impl\UsuarioLdapServiceImpl.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       0.7.1
 */